from flask import(Flask, jsonify, request, abort)

import mysql.connector

VERSION = 'v1'
BASE_URI = '/api/' + VERSION
STAFFS = '/staffs'
STAFF = 'staff'

uri_all_staffs = BASE_URI + STAFFS

app = Flask(__name__)

@app.route(uri_all_staffs, methods = ['POST'])
def add_staffs():
    if not request.json:
        abort(400)
    cnx = mysql.connector.connect(user='root', password='00000000', host='127.0.0.1', database='staffs')

    cursor = cnx.cursor()
    sql = "INSERT INTO staff (first_name, last_name, campus) values (%s,%s,%s)"

    cursor.execute(sql, (request.json['first_name'], request.json['last_name'], request.json['campus']))

    id = cursor.lastrowid
    cnx.commit()
    sql = "select * from staff where staff_id="+str(id)
    cursor.execute(sql)
    row = cursor.fetchone()
    staff = {}
    for (key, value) in zip(cursor.description,row):
        staff[key[0]] = value
    cnx.close()

    return jsonify(staff), 201

if __name__ == '__main__':
    app.run(debug=True)