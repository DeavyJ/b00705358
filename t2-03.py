def print_patter1(n):

    if n > 0:
        print(n*"*")
        print_patter1(n - 1)
    else:
        return 1


print_patter1(10)