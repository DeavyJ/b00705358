import math
class Circle:
    __radius = int
    __center_x = int
    __center_y = int

    def __init__(self, rad, x, y):
        self.__radius = rad
        self.__center_x = x
        self.__center_y = y

    def area(self, rad):
        self.__radius = rad
        a = math.pi * (rad*rad)
        return "The area of the circle is: {0:.2f}.".format(a)

    def circumference(self, rad):
        self.__radius = rad
        a = 2 * math.pi * rad
        return "The circumference of the circle is: {0:.2f}.".format(a)

    def get_Top(self, rad):
        self.__radius = rad
        y = self.__center_y + rad
        x = rad - rad
        return "The coordinates of highest point are x: {}, y: {}.".format(x, y)

    def get_right(self, rad):
        self.__radius = rad
        x = self.__center_x + rad
        x = -x
        y = rad - rad
        return "The coordinates of the most left point are x: {}, y: {}.".format(x, y)

m_circle = Circle(4, 5, 6)

print (m_circle.area(7))
print (m_circle.circumference(8))
print (m_circle.get_Top(20))
print (m_circle.get_right(3))