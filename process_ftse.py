c_change = {}
s_v = {}
c_price = {}
price = []
company_name = []
change = []
symbol = []
volume = []
lowest_price = str
minimun = str
maximum = str
total_aver = str

def open_file_nfetch():
    with open("FTSE_100.txt", 'r') as doc:
        header = doc.readline()
        details = doc.read()
        c = 0
    for lines in details.splitlines():
        i = 0
        while i != 1:
            split = lines.split('\t')
            if len(split[2]) > 0:
                split[2] = split[2].replace('"','')
                split[2] = split[2].replace(',','')
                split[5] = split[5].replace('"', '')
                split[5] = split[5].replace(',', '')
                company_name.append(split[1])
                change.append(split[3])
                symbol.append(split[0])
                volume.append(split[5])
                price.append(split[2])
                s_v[split[0]] = float(split[5])
                c_change[split[1]] = float(split[3])
                c_price[split[1]] = float(split[2])
            c += 1
            i = 1

open_file_nfetch()


def find_min_max_av():
    average = 0.00
    counter = 0
    min_ = 999.00
    max_ = 0.00
    for key, value in c_price.items():
        if value < min_:
            min_ = value
            global lowest_price
            lowest_price = ("The company with lowest price is: {}, its price is: {}.".format(key, min_))

    for key, value in c_change.items():
        average += value
        counter += 1
        if value < min_:
            min_ = value
            global minimun
            minimun = ("The company with minimum change is: {}, its change is: {}.".format(key, min_))
        if value > max_:
            max_ = value
            global maximum
            maximum = ("The company with maximum change is: {}, its change is: {}.".format(key, max_))

    global total_aver
    total_aver = ("The average for change is: {0:.2f}, there is no company to its name.\n".format(average / counter))

find_min_max_av()

print (lowest_price)
print (minimun)
print (maximum)
print (total_aver)
print (symbol[3:5])
print ("")
for key, value in s_v.items():
    symbol_volume = ("This is the symbol: {}, and this is respective volume: {}.".format(key, value))
    print (symbol_volume)