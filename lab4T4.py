import random

trick = "Snake Eye!"
count = 0
all_t = []

for rolls in range(1, 51):
    a = random.randint(1, 6)
    b = random.randint(1, 6)
    if a == b:
        all_t.append(rolls)
        print ("Loop: {}: {} & {} - {}".format(rolls, a, b, trick))
        count += 1
    else:
        print ("Loop: {}: {} & {}".format(rolls, a, b))
print ("Number of times Snake eyes is rolled is: {}.".format(count))
print ("The rolls where tricks where found in ascending order are: {}.".format(all_t))

desc = False
while not desc:
    desc = True
    for i in range(len(all_t) - 1):
        if all_t[i] < all_t[i + 1]:
            desc = False
            all_t[i], all_t[i + 1] = all_t[i + 1], all_t[i]
print ("The rolls where tricks where found in descending order are: {}.".format(all_t))