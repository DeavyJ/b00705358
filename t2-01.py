import random

def toss_coin(n):
    a = 0

    count0 = 0
    count1 = 0

    for i in range(n):
        a = random.randint(0, 1)

        if a == 0:
            count0 += 1
        elif a == 1:
            count1 += 1
    print("The amount of zero is: {}, and the amount of 1 is: {}".format(count0, count1))
    return

toss_coin(12)







