from flask import(Flask, jsonify, request, abort)

import mysql.connector

VERSION = 'v1'
BASE_URI = '/api/' + VERSION
STAFFS = '/staffs'
STAFF = 'staff'

uri_all_staffs = BASE_URI + STAFFS

app = Flask(__name__)

@app.route(uri_all_staffs, methods = ['GET'])
def get_staffs():
    cnx = mysql.connector.connect(user='root', password='00000000', host='127.0.0.1', database='staffs')

    cursor = cnx.cursor()
    query = ('SELECT * FROM staff')
    cursor.execute(query)
    rows = cursor.fetchall()
    items = []
    for row in rows:
        dict = {}
        for (key, value) in zip(cursor.description,row):
            dict[key[0]] = value
        items.append(dict)
    cnx.close()

    return jsonify ({'staffs':items}),200

if __name__ == '__main__':
    app.run(debug=True)
