class Car:
    __year_model = ""
    __make = ""
    __speed = 0

    def __init__(self, y_model, make):
        self.__year_model = y_model
        self.__make = make
        self.__speed = 0

    def accelerate(self, speed_step):
        self.__speed += speed_step
        return "You have just accelerated: {}mph to the actual.".format(speed_step)

    def brake(self, reduce_speed_step):
        if self.__speed > 0:
            self.__speed -= reduce_speed_step
            if self.__speed < 0:
                self.__speed += reduce_speed_step
                return "Trying to brake: {}, but speed is only: {}. Cannot go negative, please try again!".format(reduce_speed_step, self.__speed)
            return "You have just reduced the speed by: {}mph from the actual speed.".format(reduce_speed_step)

    def get_speed(self):
        return "Your actual speed is: {}mph.".format(self.__speed)

m_car = Car("1998", "Toyota")
m_car.accelerate(20)
m_car.accelerate(40)
m_car.accelerate(60)
m_car.accelerate(90)
print (m_car.accelerate(30))
print (m_car.get_speed())
m_car.brake(20)
m_car.brake(30)
m_car.brake(80)
m_car.brake(5)
print (m_car.brake(10))
print (m_car.get_speed())
