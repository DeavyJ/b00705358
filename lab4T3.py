import random

trick = "Snake Eye!"
count = 0

for rolls in range(1, 51):
    a = random.randint(1, 6)
    b = random.randint(1, 6)
    if a == b:
        count += 1
        print ("Loop: {}: {} & {} - {}".format(rolls, a, b, trick))
    else:
        print ("Loop: {}: {} & {}".format(rolls, a, b))
print ("Number of times Snake eyes is rolled is: {}.".format(count))
