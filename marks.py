s_name = []
s_grade = []
h_mark = 0.00
count = 0.00
total = 0.00
small = 100
with open("mark.txt", "r") as s_file:
    s_marks = s_file.read()

for lines in s_marks.splitlines():
    student_m = lines.split()
    s_grade.append(float(student_m[1]))
    s_name.append(student_m[0])
    count += 1

print("Here are all the marks: {}.".format(s_grade))

for marks in s_grade:
    if h_mark < marks:
        h_mark = marks
    total += marks

s_avg = total/count

for i in range(len(s_grade)):
    if h_mark == s_grade[i]:
        print("The student with highest mark is: {}.".format(s_name[i]))

for marks in s_grade:
    if s_avg > marks:
        print("{} is bellow average!".format(marks))

asc = False
while not asc:
    asc = True
    for i in range(len(s_grade) - 1):
        if s_grade[i] > s_grade[i + 1]:
            asc = False
            s_grade[i], s_grade[i + 1] = s_grade[i + 1], s_grade[i]
            s_name[i], s_name[i + 1] = s_name[i + 1], s_name[i]
for i in range(len(s_grade) - 1):
    print ("Grade: {}, Student: {}.".format(s_grade[i], s_name[i]))

print ("Sorted marks from lowest to highest: {}.".format(s_grade))